module PalindromeNum4 where

palindrome = maximum [a | b<-[100..999], c<-[b..999], let a=b*c, let s=show a, s==reverse s]
main = print $ palindrome