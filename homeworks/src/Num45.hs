module Num45 where
import qualified Data.Set as Set

tri :: [Int]
tri = [(n*(n + 1)) `quot` 2 | n <- [1..]]

pent :: [Int]
pent= [(n*(3*n - 1)) `quot` 2 | n <- [1..100000]]

hex :: [Int]
hex = [n*(2*n - 1) | n <- [1..100000]]

result :: [Int]
result = dropWhile (<= 40755) [t | t <- tri, isPentagonal t, isHexagonal t]
    where isPentagonal = (`Set.member` Set.fromList pent)
          isHexagonal = (`Set.member` Set.fromList hex)

main :: IO ()
main = print $ head result