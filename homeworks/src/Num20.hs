module Num20 where

fact 0 = 1
fact 1 = 1
fact n = n * fact (n - 1)

sum' :: [Integer] -> Integer
sum' [] = 0
sum' (a:as) = a + sum' as

sumOfFact n = sum' (dividedByTheNumb (fact n))

dividedByTheNumb :: Integer -> [Integer]
dividedByTheNumb x = dividedByTheNumb' x [] where
      dividedByTheNumb' :: Integer -> [Integer] -> [Integer]
      dividedByTheNumb' 0 a = a
      dividedByTheNumb' y a = dividedByTheNumb' (y `div` 10) ((y `mod` 10) : a)