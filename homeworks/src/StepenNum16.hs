module StepenNum16 where

powerOfTwo n = sum' (stepen (2^n))

stepen :: Integer -> [Integer]
stepen x = stepen' x [] where
      stepen' :: Integer -> [Integer] -> [Integer]
      stepen' 0 a = a
      stepen' y a = stepen' (y `div` 10) ((y `mod` 10) : a)

sum' :: [Integer] -> Integer
sum' [] = 0
sum' (a:as) = a + sum' as