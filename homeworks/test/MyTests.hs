-- Your tests go here

module MyTests where

import Test.Tasty (TestTree(..), testGroup)
import Test.Tasty.HUnit
import Test.Tasty.QuickCheck

import Lib

myTests :: [TestTree]
myTests =   [testGroup "HWs"
                [ testCase "Works on Alice" $ hw0_0 "Alice" @?= "Hello, Alice" ],
            testGroup "groupBy" [
                 testCase "groupBy (>)" $ groupBy' (>) [4,3,4,2,1] @?= [[4,3],[4,2,1]],
                 testCase "groupBy (==)" $ groupBy' (==) [1,1,1,2,1,1,2,2,2,1,2] @?= [[1,1,1],[2],[1,1],[2,2,2],[1],[2]],
                 testCase "groupBy (>)" $ groupBy' (>) [4,2,3,1,3,5,2,3,2,1] @?= [[4,2,3,1,3],[5,2,3,2,1]],
                 testCase "groupBy one element" $ groupBy' (>) [1] @?= [[1]]
                 ],
            testGroup "foldr" [
                       testCase "foldr mine" $ foldr' (:) [[1]] [[2],[5],[4]] @?= [[2],[5],[4],[1]]
                 ],
            testGroup "Sem1 eval_1" [
                       testCase "eval_1 (\\x . x) y == y" $ eval_1 (Apply (Lambda (TVar "x") (Var "x")) (Var "y")) @?= Just (Var "y"),

                       testCase "eval_1 (\\x . \\y . x y) (\\z . z) == (\\y . (\\z . z) y)"
                                $ eval_1 (Apply (Lambda (TVar "x")
                                                       (Lambda (TVar "y") (Apply (Var "x") (Var "y"))))
                                               (Lambda (TVar "z") (Var "z")))
                                        @?= Just (Lambda (TVar "y") (Apply (Lambda (TVar "z") (Var "z")) (Var "y"))),

                       testCase "eval_1 (\\x . \\y . x y) (\\z . z) (\\u . \\v . v) == (\\y . (\\z . z) y) (\\u . \\v . v)"
                                $ show (eval_1 (Apply (Apply (Lambda (TVar "x")
                                                       (Lambda (TVar "y") (Apply (Var "x") (Var "y"))))
                                               (Lambda (TVar "z") (Var "z")))
                                            (Lambda (TVar "u") (Lambda (TVar "v") (Var "v")))))
                                        @?= "Just (\\y . (\\z . z) y) (\\u . (\\v . v))",

                       testCase "eval_1 (\\x . (\\x . x) x) == (\\x . (\\x . x) x)"
                                $ show (eval_1 (Lambda (TVar "x") (Apply (Lambda (TVar "x") (Var "x")) (Var "x"))))
                                    @?= "Just (\\x . (\\x . x) x)",

                       testCase "eval_1 (\\x . \\y . x) y = (\\y . a)"
                                $ show (eval_1 (Apply (Lambda (TVar "x") (Lambda (TVar "y") (Var "x"))) (Var "y")))
                                    @?= "Just (\\y . a)",

                       testCase "eval_1 (\\y. \\x . y) (\\y . x) = (\\x . (\\y . a))"
                                $ eval_1 (Apply (Lambda (TVar "y") (Lambda (TVar "x") (Var "y"))) (Lambda (TVar "y") (Var "x")))
                                    @?= Just (Lambda (TVar "x") (Lambda (TVar "y") (Var "a"))),

                       testCase "eval_1 (\\x. \\y . x) (\\y . y) = (\\y . (\\a . a))"
                                $ eval_1 (Apply (Lambda (TVar "x") (Lambda (TVar "y") (Var "x"))) (Lambda (TVar "y") (Var "y")))
                                    @?= Just (Lambda (TVar "y") (Lambda (TVar "a") (Var "a"))),

                       testCase "eval_1 (\\y . (\\y . x y) y) z = (\\y . x y) z"
                                $ eval_1 (Apply (Lambda (TVar "y")
                                                (Apply (Lambda (TVar "y") (Apply (Var "x") (Var "y"))) (Var "y"))) (Var "z"))
                                    @?= Just (Apply (Lambda (TVar "y") (Apply (Var "x") (Var "y"))) (Var "z"))
                 ],
            testGroup "Sem1 eval_" [
                       testCase "eval_ (\\x . x) y == y" $ eval_ (Apply (Lambda (TVar "x") (Var "x")) (Var "y")) @?= Var "y",

                       testCase "eval_ (\\x . \\y . x y) (\\z . z) == (\\y . (\\z . z) y)"
                                $ eval_ (Apply (Lambda (TVar "x")
                                                       (Lambda (TVar "y") (Apply (Var "x") (Var "y"))))
                                               (Lambda (TVar "z") (Var "z")))
                                        @?= Lambda (TVar "y") (Apply (Lambda (TVar "z") (Var "z")) (Var "y")),

                       testCase "eval_ (\\x . \\y . x y) (\\z . z) (\\u . \\v . v) == (\\u . \\v . v)"
                                $ eval_ (Apply (Apply (Lambda (TVar "x")
                                                       (Lambda (TVar "y") (Apply (Var "x") (Var "y"))))
                                               (Lambda (TVar "z") (Var "z")))
                                            (Lambda (TVar "u") (Lambda (TVar "v") (Var "v"))))
                                        @?= Lambda (TVar "u") (Lambda (TVar "v") (Var "v")),

                       testCase "eval_ (\\x . (\\x . x) x) == (\\x . (\\x . x) x)"
                                $ show (eval_ (Lambda (TVar "x") (Apply (Lambda (TVar "x") (Var "x")) (Var "x"))))
                                    @?= "(\\x . (\\x . x) x)",

                       testCase "eval_ (\\x . \\y . x) y = (\\y . a)"
                                $ show (eval_ (Apply (Lambda (TVar "x") (Lambda (TVar "y") (Var "x"))) (Var "y")))
                                    @?= "(\\y . a)",

                       testCase "eval_ (\\x . \\a . x a) a = (\\a . b a)"
                                $ show (eval_ (Apply (Lambda (TVar "x")
                                            (Lambda (TVar "a") (Apply (Var "x") (Var "a")))) (Var "a")))
                                    @?= "(\\a . b a)",

                       testCase "eval_ (\\y. \\x . y) (\\y . x) = (\\x . (\\y . a))"
                                $ eval_ (Apply (Lambda (TVar "y") (Lambda (TVar "x") (Var "y"))) (Lambda (TVar "y") (Var "x")))
                                    @?= Lambda (TVar "x") (Lambda (TVar "y") (Var "a")),

                       testCase "eval_ (\\x. \\y . x) (\\y . y) = (\\y . (\\a . a))"
                                $ eval_ (Apply (Lambda (TVar "x") (Lambda (TVar "y") (Var "x"))) (Lambda (TVar "y") (Var "y")))
                                    @?= Lambda (TVar "y") (Lambda (TVar "a") (Var "a")),

                       testCase "eval_ (\\a . \\x . (\\b . b y) a x) (\\a . \\b . b z) = (\\x . (\\b . b y) (\\a . \\c . c z) x)"
                                $ show (eval_ (Apply
                                  	    (Lambda (TVar "a")
                                  	        (Lambda (TVar "x")
                                  	          (Apply (Apply (Lambda (TVar "b") (Apply (Var "b") (Var "y"))) (Var "a")) (Var "x"))))
                                  	(Lambda (TVar "a") (Lambda (TVar "b")
                                  				    (Apply (Var "b") (Var "z"))))))
                                     @?= "(\\x . (\\b . b y) (\\a . (\\c . c z)) x)",

                       testCase "eval_ (\\a . \\x . b a x) (\\a . \\b . b z) = (\\x . b (\\a . \\b . b z) x)"
                                  $ eval_ (Apply
                                            (Lambda (TVar "a") (Lambda (TVar "x") (Apply (Var "b") (Apply (Var "a") (Var "x")))))
                                            (Lambda (TVar "a") (Lambda (TVar "b") (Apply (Var "b") (Var "z")))))
                                    @?= Lambda (TVar "x") (Apply (Var "b") (Apply
                                                            (Lambda (TVar "a") (Lambda (TVar "b") (Apply (Var "b") (Var "z"))))
                                                            (Var "x"))),

                       testCase "eval_ (\\y . \\a . y a) (\\a . a y) = (\\a . (\\b . b y) a)"
                                $ eval_ (Apply
                                            (Lambda (TVar "y") (Lambda (TVar "a") (Apply (Var "y") (Var "a"))))
                                            (Lambda (TVar "a") (Apply (Var "a") (Var "y"))))
                                    @?= Lambda (TVar "a") (Apply (Lambda (TVar "b") (Apply (Var "b") (Var "y"))) (Var "a")),

                       testCase "eval_ (\\x . \\y . x x y) (\\a . a y) b = c c b"
                                $ show (eval_ (Apply
                                            (Apply (Lambda (TVar "x") (Lambda (TVar "y") (Apply (Apply (Var "x") (Var "x")) (Var "y"))))
                                                    (Lambda (TVar "a") (Apply (Var "a") (Var "y"))))
                                            (Var "b")))
                                    @?= "b b b",
                       testCase "eval_ (\\m . \\s . \\z . m s z) (\\s . \\z . s z)"
                                $ show (eval_
                                            (Apply (Lambda (TVar "m")
                                                                (Lambda (TVar "s")
                                                                    (Lambda (TVar "z")
                                                                        (Apply (Apply (Var "m") (Var "s")) (Var "z")))))
                                                    (Lambda (TVar "s") (Lambda (TVar "z") (Apply (Var "s") (Var "z"))))))
                                            @?= "(\\s . (\\z . (\\a . (\\b . a b)) s z))",
                       testCase "eval_ (\\m . \\n . \\s . \\z . m (n s) z) (\\s . \\z . s z) (\\s . \\z . s z) S Z"
                                $ show (eval_ (Apply
                                        (Apply (
                                            Apply (Apply (Lambda (TVar "m")
                                                            (Lambda (TVar "n")
                                                                (Lambda (TVar "s")
                                                                    (Lambda (TVar "z")
                                                                        (Apply (Apply (Var "m") (Apply (Var "n") (Var "s"))) (Var "z"))))))
                                                    (Lambda (TVar "s") (Lambda (TVar "z") (Apply (Var "s") (Var "z")))))
                                                (Lambda (TVar "s") (Lambda (TVar "z") (Apply (Var "s") (Var "z")))))
                                            (Var "S"))
                                        (Var "Z")))
                                            @?= "S Z",
                       testCase "eval_ (\\m . \\n . \\s . \\z . m (n s) z) (\\s . \\z . s (s z)) (\\s . \\z . s z) S Z"
                                $ eval_ (Apply
                                        (Apply (
                                            Apply (Apply (Lambda (TVar "m")
                                                            (Lambda (TVar "n")
                                                                (Lambda (TVar "s")
                                                                    (Lambda (TVar "z")
                                                                        (Apply (Apply (Var "m") (Apply (Var "n") (Var "s"))) (Var "z"))))))
                                                    (Lambda (TVar "s") (Lambda (TVar "z") (Apply (Var "s") (Apply (Var "s") (Var "z"))))))
                                                (Lambda (TVar "s") (Lambda (TVar "z") (Apply (Var "s") (Var "z")))))
                                            (Var "S"))
                                        (Var "Z"))
                                            @?= Apply (Var "S") (Apply (Var "S") (Var "Z")),
                       testCase "eval_ (\\c . S c) (S Z) = S (S Z)"
                                $ eval_ (Apply (Lambda (TVar "c") (Apply (Var "S") (Var "c")))
                                                     (Apply (Var "S") (Var "Z")))
                                            @?= Apply (Var "S") (Apply (Var "S") (Var "Z"))
                 ]
            ]
